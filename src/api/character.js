import axios from 'axios'
import { BASE_URL } from './base_url'

export const getCharacters = async (params) =>
  await axios.get(`${BASE_URL}/character`, { params })
  
export const getOneCharacter = async (id) =>
  await axios.get(`${BASE_URL}/character/${id}`)

export const getCharactersById = async (idsString) =>
  await axios.get(`${BASE_URL}/character/${idsString.join(',')}`)
