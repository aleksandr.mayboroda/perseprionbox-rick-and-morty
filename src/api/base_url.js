import axios from 'axios'
export const BASE_URL = 'https://rickandmortyapi.com/api/'

export const getReturnBatchData = async (urls) => {
  try{
    const allRequest = await axios.all(urls.map((oneString) => axios.get(oneString)))
    if(allRequest.length > 0)
    {
      return allRequest.filter((obj) => obj.status === 200).map((obj) => obj.data)
    }
  }
  catch(err)
  {
    console.log('trouble with server')
  }
}

