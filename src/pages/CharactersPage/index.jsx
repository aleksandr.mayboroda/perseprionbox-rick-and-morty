import React from 'react'
import { Box } from '@mui/material'

import SearchChip from '../../components/chunks/SearchChip/SearchChip'
import Search from '../../components/chunks/Search'
import CharacterList from '../../components/containers/CharacterList'

const CharactersPage = () => {
  return (
    <Box>
      <Box sx={{display: 'flex', justifyContent: 'flex-end'}}>
        <SearchChip />
        <Search />
      </Box>
      <CharacterList />
    </Box>
  )
}

export default CharactersPage
