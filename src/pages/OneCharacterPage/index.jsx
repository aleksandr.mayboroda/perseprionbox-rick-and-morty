import React from 'react'
import { useParams } from 'react-router-dom'

import OneCharacterInfo from '../../components/containers/OneCharacterInfo'

const OneCharacterPage = () => {
  const { id } = useParams()

  return <OneCharacterInfo id={id} />
}

export default OneCharacterPage
