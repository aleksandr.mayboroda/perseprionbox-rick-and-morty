import React from 'react'

import ActivityTabs from '../../components/chunks/ActivityTabs/ActivityTabs'
import ActivityTabsContent from '../../components/chunks/ActivityTabsContent/ActivityTabsContent'

const ActivityPage = () => {
  return (
    <>
      <ActivityTabs />
      <ActivityTabsContent />
    </>
  )
}

export default ActivityPage
