import React, { PureComponent } from 'react'
import { Box, Typography, styled } from '@mui/material'

import image from './boundary.jpg'

const Block = styled(Box)`
  background: url(${image}) no-repeat center;
  background-clip: border-box;
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  padding-left: 1rem;
  align-items: center;
`
const Text = styled(Typography)`
  color: #fff;
  font-size: 30px;
  text-transform: uppercase;
  letter-spacing: 2px;
  animation: runningText 3s linear both;
  animation-delay: 0;

  @keyframes runningText {
    0% {
      margin-left: -500px;
    }
    50% {
      margin-left: -250px;
    }
    100% {
      margin-left: 50px;
    }
  }
`

class ErrorBoundary extends PureComponent {
  state = {
    isError: null,
  }

  static getDerivedStateFromError(error) {
    return { isError: error }
  }

  render() {
    if (this.state.isError) {
      return (
        <Block>
          <Text>Something went wrong...</Text>
        </Block>
      )
    }
    return this.props.children
  }
}

export default ErrorBoundary
