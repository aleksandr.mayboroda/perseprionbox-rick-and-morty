import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: '#F4E041',
    },
    secondary: {
      main: '#12b0c9',
    },
    background: {
      paper: '#121212',
      default: '#1f1f1f',
    },
    border: { main: '#fff' },
    error: { main: '#ff6347' },
    text: {
      primary: '#fff',
      secondary: '#fff', //autocomplete text color
      disabled: '#fff',
      light: '#fff',
    },
    action: {
      disabled: '#fff', //color
      active: '#fff',
    },
  },
  shape: {
    borderRadius: 7,
  },
  typography: {
    fontFamily: 'Roboto, sans-serif',
    htmlFontSize: 16,
    fontSize: 16,
    fontWeight: 400,
    fontWeightRegular: 400,
    color: '#fff',
    h1: {
      fontSize: 50,
    },
  },

  components: {},
})

export default theme
