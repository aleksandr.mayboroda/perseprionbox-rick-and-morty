import React from 'react'
import AppRoutes from './routes/AppRoutes'
import { Box } from '@mui/system'

import LayoutMain from './components/ui/LayoutMain/LayoutMain'
import Header from './components/containers/Header'

const App = () => {
  return (
    <LayoutMain>
      <Header />
      <Box sx={{ flexGrow: 1, minHeight: 500 }}>
        <AppRoutes />
      </Box>
    </LayoutMain>
  )
}

export default App
