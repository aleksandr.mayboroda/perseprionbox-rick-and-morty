const getPage = () => (state) => state.character.page
const getIsLoading = () => (state) => state.character.isLoading
const checkIsLiked = (id) => (state) =>
  state.character.liked.find((elem) => elem === id)
const checkIsDisLiked = (id) => (state) =>
  state.character.disliked.find((elem) => elem === id)
const activityCounter = () => (state) =>
  state.character.liked.length + state.character.disliked.length
const getLikes = () => (state) => state.character.liked
const getDisLikes = () => (state) => state.character.disliked
const getSearchLine = () => (state) => state.character.search

const def = {
  getPage,
  getIsLoading,
  checkIsLiked,
  checkIsDisLiked,
  activityCounter,
  getLikes,
  getDisLikes,
  getSearchLine,
}

export default def
