import types from './types'

const initialState = {
  page: +localStorage.getItem('characterPage') || 1,
  liked: JSON.parse(localStorage.getItem('characterLiked')) || [],
  disliked: JSON.parse(localStorage.getItem('characterDisliked')) || [],
  isLoading: false,
  search: '',
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_PAGE: {
      const page = action.payload
      localStorage.setItem('characterPage', page)
      return { ...state, page }
    }
    case types.SET_IS_LOADING: {
      return { ...state, isLoading: action.payload }
    }
    case types.TOGGLE_LIKED: {
      const id = action.payload
      let newLikedArr = []
      let newDisLikedArr = state.disliked.filter((elem) => elem !== id) //it can't be liked or disliked at the same time
      const exists = state.liked.find((item) => item === id)
      newLikedArr = exists
        ? state.liked.filter((elem) => elem !== id)
        : [...state.liked, id]

      localStorage.setItem('characterLiked', JSON.stringify(newLikedArr))
      localStorage.setItem('characterDisliked', JSON.stringify(newDisLikedArr))
      return { ...state, liked: newLikedArr, disliked: newDisLikedArr }
    }
    case types.TOGGLE_DISLIKED: {
      const id = action.payload
      let newDisLikedArr = []
      let newLikedArr = state.liked.filter((elem) => elem !== id) //it can't be liked or disliked
      const exists = state.disliked.find((item) => item === id)
      newDisLikedArr = exists
        ? state.disliked.filter((elem) => elem !== id)
        : [...state.disliked, id]
      localStorage.setItem('characterLiked', JSON.stringify(newLikedArr))
      localStorage.setItem('characterDisliked', JSON.stringify(newDisLikedArr))
      return { ...state, liked: newLikedArr, disliked: newDisLikedArr }
    }
    case types.CLEAR_LIKED: {
      const liked = []
      localStorage.setItem('characterLiked', JSON.stringify(liked))
      return { ...state, liked }
    }
    case types.CLEAR_DISLIKED: {
      const disliked = []
      localStorage.setItem('characterDisliked', JSON.stringify(disliked))
      return { ...state, disliked }
    }
    case types.SET_SEARCH: {
      return { ...state, search: action.payload }
    }
    default:
      return state
  }
}

export default reducer
