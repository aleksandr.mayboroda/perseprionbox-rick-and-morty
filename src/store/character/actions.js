import types from './types'

const setPage = page => ({
  type: types.SET_PAGE,
  payload: page,
})

const setIsLoading = value => ({
  type: types.SET_IS_LOADING,
  payload: value,
})

const toggleLiked = id => ({
  type: types.TOGGLE_LIKED,
  payload: id,
})

const toggleDisLiked = id => ({
  type: types.TOGGLE_DISLIKED,
  payload: id,
})

const clearLiked = () => ({
  type: types.CLEAR_LIKED,
})

const clearDisLiked = () => ({
  type: types.CLEAR_DISLIKED,
})

const setSearch = (newSearch) => ({
  type: types.SET_SEARCH,
  payload: newSearch,
})


const def = {
  setPage,
  setIsLoading,
  toggleLiked,
  toggleDisLiked,
  clearLiked,
  clearDisLiked,
  setSearch,
}

export default def