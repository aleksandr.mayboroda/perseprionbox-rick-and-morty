import actions from './actions'

const clearSearch = () => (dispatch, getState) => {
  dispatch(actions.setSearch(''))
  dispatch(actions.setPage(1))
}

const def = { ...actions, clearSearch }

export default def
