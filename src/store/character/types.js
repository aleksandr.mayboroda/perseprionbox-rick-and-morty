const SET_PAGE = 'rick_and_morty/character/SET_PAGE'
const SET_IS_LOADING = 'rick_and_morty/character/SET_IS_LOADING'
const TOGGLE_LIKED = 'rick_and_morty/character/SET_LIKED'
const TOGGLE_DISLIKED = 'rick_and_morty/character/SET_DISLIKED'
const CLEAR_LIKED = 'rick_and_morty/character/CLEAR_LIKED'
const CLEAR_DISLIKED = 'rick_and_morty/character/CLEAR_DISLIKED'
const SET_SEARCH = 'rick_and_morty/character/SET_SEARCH'

const def = {
  SET_PAGE,
  SET_IS_LOADING,
  TOGGLE_LIKED,
  TOGGLE_DISLIKED,
  CLEAR_LIKED,
  CLEAR_DISLIKED,
  SET_SEARCH,
}
export default def
