import { styled } from '@mui/material'

const SpanTextTitle = styled('span')(({ theme }) => ({
  color: theme.palette.secondary.main,
  fontSize: 'smaller',
}))

export default SpanTextTitle