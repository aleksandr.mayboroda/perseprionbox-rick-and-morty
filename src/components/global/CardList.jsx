import { Box, styled } from '@mui/material'

const CardList = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexWrap: 'wrap',
  [theme.breakpoints.down('sm')]: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  [theme.breakpoints.only('sm')]: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  [theme.breakpoints.up('md')]: {
    justifyContent: 'start',
  },
}))

export default CardList
