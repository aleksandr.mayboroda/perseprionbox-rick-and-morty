import {memo} from 'react'
import { Typography } from '@mui/material'

const EmptyListMessage = ({ message }) => {
  return (
    <Typography variant="h6" align="center" sx={{ p: 10 }}>
      {message}
    </Typography>
  )
}

export default memo(EmptyListMessage)
