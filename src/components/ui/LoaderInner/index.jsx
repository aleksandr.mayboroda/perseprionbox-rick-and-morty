import { memo } from 'react'
import { CircularProgress } from '@mui/material'

const LoaderInner = ({ loading }) => {
  return (
    <>
      {loading ? (
        <CircularProgress color="secondary" size={20} sx={{ zIndex: 6 }} />
      ) : null}
    </>
  )
}

export default memo(LoaderInner)
