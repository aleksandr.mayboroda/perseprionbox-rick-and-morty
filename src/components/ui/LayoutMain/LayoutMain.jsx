import React from 'react'
import { Box, Container } from '@mui/system'

const LayoutMain = ({ children }) => {
  return (
    <Container maxWidth="lg" sx={{ backgroundColor: 'background.paper' }}>
      <Box sx={{ display: 'flex', flexDirection: 'column' }}>{children}</Box>
    </Container>
  )
}

export default LayoutMain
