import { memo } from 'react'
import { Backdrop, CircularProgress } from '@mui/material'

const Loader = ({ isOpen }) => {
  return (
    <Backdrop open={isOpen} sx={{ color: 'button', zIndex: 3 }}>
      <CircularProgress color="secondary" sx={{ zIndex: 6 }} />
    </Backdrop>
  )
}

export default memo(Loader)
