import { memo } from 'react'
import { Box } from '@mui/material'

import Activity from '../../chunks/Activity'
import Logo from '../../chunks/Logo'

const Header = () => {
  return (
    <Box sx={{ py: 3 }}>
      <Box sx={{ display: 'flex', justifyContent: 'end' }}>
        <Activity />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'center' }}>
        <Logo />
      </Box>
    </Box>
  )
}

export default memo(Header)
