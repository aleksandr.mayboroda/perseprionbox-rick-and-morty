import { useState, useEffect, useCallback, memo } from 'react'
import { Box, Grid, Typography } from '@mui/material'
import { getOneCharacter } from '../../../api/character'

import Loader from '../../ui/Loader'
import OneCharacterImage from '../../chunks/OneCharacterImage'
import OneCharacterData from '../../chunks/OneCharacterData'
import OneCharacterEpisodesList from '../../chunks/OneCharacterEpisodesList'

const OneCharacterInfo = ({ id }) => {
  const [charData, setCharData] = useState(null)
  const [loading, setLoading] = useState(false)

  const loadCharacterData = useCallback(async () => {
    setLoading(true)
    try {
      const response = await getOneCharacter(id)
      if (response.status === 200) {
        setCharData(response.data)
      } else {
        setCharData(null)
      }
    } catch (err) {
      console.error('server error')
      setCharData(null)
    }
    setLoading(false)
  }, [id])

  useEffect(() => {
    loadCharacterData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Box>
      <Loader isOpen={loading} />
      {!charData && (
        <Typography variant="h6" align="center" sx={{ p: 10 }}>
          No character data is found
        </Typography>
      )}
      {charData && (
        <>
          <Grid container sx={{ py: 10 }}>
            <Grid item xs={12} md={6}>
              <OneCharacterImage {...charData} />
            </Grid>
            <Grid
              item
              xs={12}
              md={6}
              sx={{ textAlign: 'center', px: { xs: 3 } }}
            >
              <OneCharacterData {...charData} />
            </Grid>
            {charData.episode.length > 0 && (
              <OneCharacterEpisodesList episodesUrls={charData.episode} />
            )}
          </Grid>
        </>
      )}
    </Box>
  )
}

export default memo(OneCharacterInfo)
