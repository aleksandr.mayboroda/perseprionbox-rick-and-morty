import { useState, useEffect, useCallback, memo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  characterSelectors,
  characterOperations,
} from '../../../store/character'
import { Box } from '@mui/material'

import { getCharactersById } from '../../../api/character'

import CancelPresentationRoundedIcon from '@mui/icons-material/CancelPresentationRounded'

import CustomBadge from '../../chunks/CustomBadge'
import CardList from '../../global/CardList'
import OneCharacterCard from '../../chunks/OneCharacterCard'
import Loader from '../../ui/Loader'
import EmptyListMessage from '../../global/EmptyListMessage'

const Dislikes = () => {
  const dispatch = useDispatch()

  const dislikedElems = useSelector(characterSelectors.getDisLikes())
  const counter = dislikedElems.length

  const [dislikedCharacterList, setDisLikedCgaracterList] = useState([])
  const [loading, setLoading] = useState(false)

  const clearDisLikes = useCallback(() => {
    setLoading(true)
    dispatch(characterOperations.clearDisLiked())
    setDisLikedCgaracterList([])
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const loadCharacters = useCallback(async () => {
    setLoading(true)
    if (dislikedElems.length > 0) {
      try {
        const response = await getCharactersById(dislikedElems)
        if (response.status === 200) {
          setDisLikedCgaracterList(Array.isArray(response.data) ? response.data : [response.data])
        } else {
          setDisLikedCgaracterList([])
        }
      } catch (err) {}
    } else {
      setDisLikedCgaracterList([])
    }
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dislikedElems])

  useEffect(() => {
    loadCharacters()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dislikedElems])

  const list =
    !dislikedCharacterList.length > 0
      ? null
      : dislikedCharacterList.map((item) => (
          <OneCharacterCard key={item.id} {...item} />
        ))

  return (
    <Box>
      <Loader isOpen={loading} />
      {counter > 0 && (
        <CustomBadge
          counter={counter}
          onClick={clearDisLikes}
          tooltip={`Clear all ${counter} dislikes`}
        >
          <CancelPresentationRoundedIcon />
        </CustomBadge>
      )}
      {list && <CardList>{list}</CardList>}
      {!dislikedElems.length > 0 && (
        <EmptyListMessage message="There's no characters disliked" />
      )}
    </Box>
  )
}

export default memo(Dislikes)
