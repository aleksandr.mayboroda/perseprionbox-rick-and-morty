import { useState, useEffect, useCallback, memo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  characterSelectors,
  characterOperations,
} from '../../../store/character'
import { Box } from '@mui/material'

import { getCharactersById } from '../../../api/character'

import CancelPresentationRoundedIcon from '@mui/icons-material/CancelPresentationRounded'

import CustomBadge from '../../chunks/CustomBadge'
import CardList from '../../global/CardList'
import OneCharacterCard from '../../chunks/OneCharacterCard'
import Loader from '../../ui/Loader'
import EmptyListMessage from '../../global/EmptyListMessage'

const Likes = () => {
  const dispatch = useDispatch()

  const likedElems = useSelector(characterSelectors.getLikes())
  const likedCounter = likedElems.length

  const [likedCharacterList, setLikedCgaracterList] = useState([])
  const [loading, setLoading] = useState(false)

  const clearLikes = useCallback(() => {
    setLoading(true)
    dispatch(characterOperations.clearLiked())
    setLikedCgaracterList([])
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const loadCharacters = useCallback(async () => {
    setLoading(true)
    if (likedElems.length > 0) {
      try {
        const response = await getCharactersById(likedElems)
        if (response.status === 200) {
          setLikedCgaracterList(
            Array.isArray(response.data) ? response.data : [response.data]
          )
        } else {
          setLikedCgaracterList([])
        }
      } catch (err) {}
    } else {
      setLikedCgaracterList([])
    }
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [likedElems])

  useEffect(() => {
    loadCharacters()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [likedElems])

  const list =
    !likedCharacterList.length > 0
      ? null
      : likedCharacterList.map((item) => (
          <OneCharacterCard key={item.id} {...item} />
        ))

  return (
    <Box>
      <Loader isOpen={loading} />
      {likedCounter > 0 && (
        <CustomBadge
          counter={likedCounter}
          onClick={clearLikes}
          tooltip={`Clear all ${likedCounter} likes`}
        >
          <CancelPresentationRoundedIcon />
        </CustomBadge>
      )}
      {list && <CardList>{list}</CardList>}
      {!likedElems.length > 0 && (
        <EmptyListMessage message="There's no characters liked" />
      )}
    </Box>
  )
}

export default memo(Likes)
