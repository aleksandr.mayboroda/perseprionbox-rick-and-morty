import { useState, useEffect, useCallback, memo } from 'react'
import { Box, Pagination, Typography } from '@mui/material'
import { getCharacters } from '../../../api/character'
import { useSelector, useDispatch } from 'react-redux'
import {
  characterSelectors,
  characterOperations,
} from '../../../store/character'

import CardList from '../../global/CardList'
import OneCharacterCard from '../../chunks/OneCharacterCard'
import Loader from '../../ui/Loader'

const CharacterList = () => {
  const dispatch = useDispatch()

  const [characterDefaultList, setCharacterDefaultList] = useState([])
  const [charactersInfo, setCharactersInfo] = useState(null)

  const isLoading = useSelector(characterSelectors.getIsLoading())
  const page = useSelector(characterSelectors.getPage())

  //search from autocomplete
  const search = useSelector(characterSelectors.getSearchLine())

  const setIsLoading = useCallback(
    (newVal) => dispatch(characterOperations.setIsLoading(newVal)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )
  const setPage = useCallback(
    (_, newPage) => dispatch(characterOperations.setPage(newPage)),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const loadCharacters = useCallback(async () => {
    setIsLoading(true)
    try {
      const newFilters = {}
      newFilters.page = page

      if(search)
      {
        newFilters.name = search
      }

      const response = await getCharacters(newFilters)
      if (response.status === 200) {
        if (response.data.results.length > 0) {
          setCharacterDefaultList(response.data.results)
          setCharactersInfo(response.data.info)
        } else {
          setCharacterDefaultList([])
          setCharactersInfo(null)
        }
      }
    } catch (err) {
      setCharacterDefaultList([])
      setCharactersInfo(null)
      console.log('trouble with server')
    }

    setIsLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, search])

  useEffect(() => {
    loadCharacters()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, search])

  const list =
    !characterDefaultList.length > 0
      ? null
      : characterDefaultList.map((item) => (
          <OneCharacterCard {...item} key={item.id} />
        ))

  const pagination = (
    <Pagination
      page={page}
      color="primary"
      shape="rounded"
      size="large"
      variant="outlined"
      onChange={setPage}
      count={charactersInfo?.pages}
      sx={{
        my: 2,
        '& .MuiPaginationItem-root': {
          borderColor: 'border.main',
          '&.Mui-selected': {
            borderColor: 'primary.main',
          },
        },
      }}
    />
  )

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Loader isOpen={isLoading} />
      {!characterDefaultList && (
        <Typography variant="h6" align="center" sx={{ p: 10 }}>
          No character is found
        </Typography>
      )}
      {list && (
        <>
          {pagination}
          <CardList>{list}</CardList>
          {pagination}
        </>
      )}
    </Box>
  )
}

export default memo(CharacterList)
