import { Typography, styled } from '@mui/material'

const StyledTypographyRow = styled(Typography)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
}))

export default StyledTypographyRow