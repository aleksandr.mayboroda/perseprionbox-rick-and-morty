import { memo } from 'react'
import { Typography } from '@mui/material'
import dayjs from 'dayjs'

import useStatusChip from '../../../hooks/useStatusChip'

import SpanTextTitle from '../../global/SpanTextTitle'
import CharRow from './CharRow'

const OneCharacterData = ({
  name,
  species,
  gender,
  location,
  status,
  created,
}) => {
  const { returnChipByStatus } = useStatusChip()
  const statusChip = returnChipByStatus(status)
  const birtDate = dayjs(created).format('D MMM YYYY')

  return (
    <>
      <Typography variant="h1" sx={{ mb: 5 }}>
        {name}
      </Typography>
      <CharRow>
        <SpanTextTitle>species: </SpanTextTitle>
        {species}
      </CharRow>
      <CharRow>
        <SpanTextTitle>gender: </SpanTextTitle>
        {gender}
      </CharRow>
      {location.name && (
        <CharRow>
          <SpanTextTitle>location: </SpanTextTitle>
          {location.name}
        </CharRow>
      )}
      <CharRow sx={{ py: 1 }}>
        <SpanTextTitle>status: </SpanTextTitle>
        {statusChip}
      </CharRow>
      <CharRow>
        <SpanTextTitle>birth: </SpanTextTitle>
        {birtDate}
      </CharRow>
    </>
  )
}

export default memo(OneCharacterData)
