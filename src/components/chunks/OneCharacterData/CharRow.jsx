import React from 'react'
import StyledTypographyRow from './StyledTypographyRow'

const CharRow = ({ children, ...props }) => {
  return (
    <StyledTypographyRow variant="h5" {...props}>
      {children}
    </StyledTypographyRow>
  )
}

export default CharRow
