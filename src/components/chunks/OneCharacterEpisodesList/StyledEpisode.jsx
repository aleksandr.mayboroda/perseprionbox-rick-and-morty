import { Typography, styled } from '@mui/material'

const StyledEpisode = styled(Typography)(({ theme }) => {
  return {
    border: `1px solid ${theme.palette.border.main}`,
    borderRadius: theme.shape.borderRadius,
    padding: 7,
    margin: 5,
  }
})

export default StyledEpisode