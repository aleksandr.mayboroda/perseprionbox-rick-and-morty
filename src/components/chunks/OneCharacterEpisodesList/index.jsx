import { useState, useEffect, useCallback, memo } from 'react'
import { Grid, Typography } from '@mui/material'
import { getReturnBatchData } from '../../../api/base_url'

import LoaderInner from '../../ui/LoaderInner'

import SpanTextTitle from '../../global/SpanTextTitle'

import StyledEpisode from './StyledEpisode'
import StyledHeader from './StyledHeader'

const OneCharacterEpisodesList = ({ episodesUrls }) => {
  const [episodeList, setEpisodeList] = useState([])
  const [loading, setLoading] = useState(false)

  const loadEpisodes = useCallback(async () => {
    setLoading(true)
    try {
      const response = await getReturnBatchData(episodesUrls)
      if (response.length > 0) {
        setEpisodeList(response)
      } else {
        setEpisodeList([])
      }
    } catch (err) {
      console.error('server error')
    }
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [episodesUrls])

  useEffect(() => {
    loadEpisodes()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const list =
    !episodeList.length > 0
      ? null
      : episodeList.map(({ id, name, episode }) => (
          <StyledEpisode key={id}>
            <StyledHeader>{episode}:</StyledHeader>
            {name}
          </StyledEpisode>
        ))

  return (
    <Grid
      container
      sx={{ mt: 5, display: 'flex', alignItems: 'start', px: { xs: 3 } }}
    >
      <Grid item xs={12} md={6}>
        <Typography
          variant="h5"
          sx={{ pr: 5, textAlign: { xs: 'left', md: 'right' }, mb: 2 }}
        >
          <SpanTextTitle>episodes:</SpanTextTitle>
        </Typography>
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        sx={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}
      >
        <LoaderInner loading={loading} />
        {list}
      </Grid>
    </Grid>
  )
}

export default memo(OneCharacterEpisodesList)
