import { styled } from '@mui/material'

const StyledHeader = styled('span')(({ theme }) => ({
  color: theme.palette.secondary.main,
  marginRight: 5,
}))

export default StyledHeader
