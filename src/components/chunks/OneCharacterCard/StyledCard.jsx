import { Card, styled } from '@mui/material'

const StyledCard = styled(Card)(({ theme }) => ({
  margin: 10,
  border: `1px solid ${theme.palette.border.main}`,
  [theme.breakpoints.only('sx')]: {
    width: '100%',
  },
  [theme.breakpoints.only('sm')]: {
    maxWidth: 'calc(100% / 2 - 20px)',
    margin: 10,
  },
  [theme.breakpoints.only('md')]: {
    maxWidth: 'calc(100% / 3 - 30px)',
  },
  [theme.breakpoints.up('lg')]: {
    maxWidth: 'calc(100% / 4 - 40px)',
    margin: 20,
  },
}))

export default StyledCard
