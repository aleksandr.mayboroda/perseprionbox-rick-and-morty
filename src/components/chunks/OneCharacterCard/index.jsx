import { memo } from 'react'
import { Box, CardMedia, CardContent, CardActions, Button } from '@mui/material'
import useStatusChip from '../../../hooks/useStatusChip'

import { Link } from 'react-router-dom'
import { CHARACTERS } from '../../../routes/consts'

import CharacterControls from '../CharacterControls'

import StyledCard from './StyledCard'
import StyledName from './StyledName'

const OneCharacterCard = ({ id, name, image, status }) => {
  const path = `/${CHARACTERS}/${id}`
  const { returnChipByStatus } = useStatusChip()
  const statusChip = returnChipByStatus(status)

  return (
    <StyledCard key={id}>
      <CardMedia component="img" alt={name} image={image} />
      <CardContent>
        <StyledName gutterBottom variant="h6" component="div" align="center">
          {name}
        </StyledName>
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          {statusChip}
        </Box>
      </CardContent>
      <CardActions sx={{ display: 'flex', justifyContent: 'space-around' }}>
        <CharacterControls id={id} />
        <Button size="small" component={Link} to={path}>
          Learn More
        </Button>
      </CardActions>
    </StyledCard>
  )
}

export default memo(OneCharacterCard)
