import { Typography, styled } from '@mui/material'

//cut string and add '...' at the end
const StyledName = styled(Typography)(() => ({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
}))

export default StyledName
