import {memo} from 'react'
import { Box, IconButton, Tooltip, Badge } from '@mui/material'

const CustomBadge = ({children, counter, onClick, tooltip}) => {
  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'end',
        alignSelf: 'end',
      }}
    >
      <Tooltip title={tooltip} color="primary">
        <Badge color="primary" badgeContent={counter}>
          <IconButton color="secondary" onClick={onClick}>
            {children}
          </IconButton>
        </Badge>
      </Tooltip>
    </Box>
  )
}

export default memo(CustomBadge)
