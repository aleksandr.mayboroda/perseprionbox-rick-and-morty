import { memo } from 'react'
import { useSelector } from 'react-redux'
import { characterSelectors } from '../../../store/character'

import { Box, Badge, Typography } from '@mui/material'
import StyledLink from './StyledLink'

const ActivityTabs = () => {
  const likedElems = useSelector(characterSelectors.getLikes())
  const dislikedElems = useSelector(characterSelectors.getDisLikes())
  const counter = likedElems.length
  const counterDis = dislikedElems.length

  return (
    <Box>
      <StyledLink to={'/activity/like'}>
        <Badge color="primary" badgeContent={counter}>
          <Typography sx={{ lineHeight: 2 }}>Likes</Typography>
        </Badge>
      </StyledLink>
      <StyledLink to={'/activity/dislike'}>
        <Badge color="primary" badgeContent={counterDis}>
          <Typography sx={{ lineHeight: 2 }}>Dislikes</Typography>
        </Badge>
      </StyledLink>
    </Box>
  )
}

export default memo(ActivityTabs)
