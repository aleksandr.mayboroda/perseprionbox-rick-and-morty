import { NavLink } from 'react-router-dom'
import { styled } from '@mui/material'

const StyledLink = styled(NavLink)(({ theme }) => ({
  textDecoration: 'none',
  backgroundColor: theme.palette.background.paper,
  padding: 20,
  color: theme.palette.text.primary,
  border: `2px solid ${theme.palette.border.main}`,
  borderTopLeftRadius: theme.shape.borderRadius,
  borderTopRightRadius: theme.shape.borderRadius,

  '&.active': {
    color: theme.palette.primary.main,
    borderBottom: 'none',
  },
}))

export default StyledLink
