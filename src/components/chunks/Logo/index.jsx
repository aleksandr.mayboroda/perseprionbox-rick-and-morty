import React from 'react'
import { Stack, Tooltip } from '@mui/material'

import imagePath from './logo.png'

import StyledLink from './StyledLink'

const Logo = () => {
  return (
    <Stack sx={{ p: 1 }} alignItems="center">
      <Tooltip title={`Go to main page`} color="primary">
        <StyledLink to="/">
          <img
            src={imagePath}
            alt="logo"
            loading="lazy"
            style={{ maxWidth: '100%' }}
          />
        </StyledLink>
      </Tooltip>
    </Stack>
  )
}

export default Logo
