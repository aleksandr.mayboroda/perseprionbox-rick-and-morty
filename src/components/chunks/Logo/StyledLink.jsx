import { Link } from 'react-router-dom'
import { styled } from '@mui/material'

const StyledLink = styled(Link)(({ theme }) => ({
  [theme.breakpoints.down('sm')]: {
    width: 230,
  },
  [theme.breakpoints.up('sm')]: {
    width: 300,
  },
}))

export default StyledLink