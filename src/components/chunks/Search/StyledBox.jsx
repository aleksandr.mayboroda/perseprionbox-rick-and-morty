import { Box, styled } from '@mui/material'

const StyledBox = styled(Box)(({ theme }) => ({
  padding: 10,
  width: '50%',
  display: 'flex',
  justifyContent: 'end',
}))

export default StyledBox
