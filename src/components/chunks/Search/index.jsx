import { useState, useEffect, memo, useCallback } from 'react'
import { TextField, Autocomplete } from '@mui/material'
import { getCharacters } from '../../../api/character'
import { useDispatch } from 'react-redux'
import { characterOperations } from '../../../store/character'

import useDebounce from '../../../hooks/useDebounce'

import AutocompleteLink from '../AutocompleteLink'
import LoaderInner from '../../ui/LoaderInner'

import StyledBox from './StyledBox'

const Search = () => {
  const dispatch = useDispatch()

  const [options, setOptions] = useState([])
  const [name, setName] = useState('')
  const [open, setOpen] = useState(false)
  const [isEnd, setIsEnd] = useState(false) //the bottom of autocomplete scroll
  const [page, setPage] = useState(1) //the page of autocomplete scroll
  const loading = !options.length && open

  const loadOptions = useCallback(async () => {
    try {
      if (!isEnd) {
        const optionsResult = await getCharacters({ name, page })

        if (optionsResult.status < 400) {
          const all = [...options, ...optionsResult.data.results]
          setOptions(all)
          setIsEnd(!!!optionsResult.data.info.next)
          if (optionsResult.data.info.next) {
            setPage((prev) => prev + 1)
          }
        } else {
          setPage(1)
          setIsEnd(false)
          setOptions([])
        }
      }
    } catch (err) {
      console.error('server error')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [name, page])

  const debouncedSetName = useDebounce((_, newValue) => {
    resetSettings()
    setName(newValue)
  }, 300)
  const debouncedLoadOptions = useDebounce(loadOptions, 500)

  const onScroll = useCallback(
    (event) => {
      const listboxNode = event.currentTarget

      const position = listboxNode.scrollTop + listboxNode.clientHeight
      if (listboxNode.scrollHeight - position <= 1) {
        debouncedLoadOptions()
      }
    },
    [debouncedLoadOptions]
  )

  const onEnter = useCallback(
    (event) => {
      if (event.key === 'Enter') {
        onClose()
        dispatch(characterOperations.setSearch(name))
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [name, characterOperations]
  )

  const resetSettings = useCallback(() => {
    setPage(1)
    setIsEnd(false)
    setOptions([])
  }, [setPage, setIsEnd, setOptions])

  const onOpen = useCallback(() => {
    setOpen(true)
  }, [setOpen])

  const onClose = useCallback(() => {
    resetSettings()
    setOpen(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resetSettings, setOpen])

  useEffect(() => {
    if (open) {
      loadOptions()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, name])

  return (
    <StyledBox>
      <Autocomplete
        noOptionsText={'No characters is found'}
        onOpen={onOpen}
        onClose={onClose}
        variant="contained"
        sx={{ width: 300 }}
        options={options}
        getOptionLabel={(option) => option.name}
        id="name"
        name={name}
        onInputChange={debouncedSetName}
        renderInput={(params) => (
          <TextField
            {...params}
            value={name}
            label="Search by name"
            variant="outlined"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {loading && <LoaderInner loading={loading} />}
                  {params.InputProps.endAdornment}
                </>
              ),
              onKeyDown: onEnter,
            }}
            sx={{
              // '& .MuiInputLabel-root': {
              //   color: 'text.primary',
              // },
              '& .MuiOutlinedInput-notchedOutline': {
                borderColor: 'border.main',
              },
            }}
          />
        )}
        renderOption={(props, item) => (
          <AutocompleteLink key={item.id} {...item} props={props} />
        )}
        //infiniti scroll functional
        ListboxProps={{
          onScroll: onScroll,
        }}
      />
    </StyledBox>
  )
}

export default memo(Search)
