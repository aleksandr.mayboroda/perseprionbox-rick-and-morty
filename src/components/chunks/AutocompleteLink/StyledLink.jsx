import { Link, styled } from '@mui/material'

const StyledLink = styled(Link)(() => ({
  display: 'flex',
  textDecoration: 'none',
  '& > img': { marginRight: 10, flexShrink: 0 },
}))

export default StyledLink
