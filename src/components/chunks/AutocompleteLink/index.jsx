import { memo } from 'react'
import { Box, Typography } from '@mui/material'
import { Link as RouterLink } from 'react-router-dom'
import useStatusChip from '../../../hooks/useStatusChip'

import StyledLink from './StyledLink'

const AutocompleteLink = ({ id, name, status, image, props }) => {
  const { returnChipByStatus } = useStatusChip()
  const charStatus = returnChipByStatus(status)

  return (
    <StyledLink
      component={RouterLink}
      {...props}
      to={`/character/${id}`}
      color="secondary"
    >
      <img loading="lazy" width="70" src={image} alt={name} />
      <Box>
        <Typography variant="p">{name}</Typography>
        {charStatus}
      </Box>
    </StyledLink>
  )
}

export default memo(AutocompleteLink)
