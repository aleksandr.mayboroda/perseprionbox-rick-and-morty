import { memo } from 'react'
import { Box } from '@mui/material'

import CharacterControls from '../CharacterControls'

const OneCharacterImage = ({ id, image, name }) => {
  return (
    <>
      <Box sx={{ display: 'flex', justifyContent: 'center' }}>
        <img src={image} alt={name} />
      </Box>
      <Box sx={{ display: 'flex', justifyContent: 'center', py: 2 }}>
        <CharacterControls id={id} />
      </Box>
    </>
  )
}

export default memo(OneCharacterImage)
