import {useCallback, memo} from 'react'
import {Box, IconButton} from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import {
  characterSelectors,
  characterOperations,
} from '../../../store/character'

import ThumbUpAltOutlinedIcon from '@mui/icons-material/ThumbUpAltOutlined'
import ThumbDownOffAltOutlinedIcon from '@mui/icons-material/ThumbDownOffAltOutlined'
import ThumbUpAltRoundedIcon from '@mui/icons-material/ThumbUpAltRounded'
import ThumbDownAltRoundedIcon from '@mui/icons-material/ThumbDownAltRounded'

const CharacterControls = ({id}) => {
  const dispatch = useDispatch()
  
  const isLiked = useSelector(characterSelectors.checkIsLiked(id))
  const isDisLiked = useSelector(characterSelectors.checkIsDisLiked(id))

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const toggleLike = useCallback(() => {dispatch(characterOperations.toggleLiked(id))},[])
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const toggleDisLike = useCallback(() => {dispatch(characterOperations.toggleDisLiked(id))},[])


  return (
    <Box>
      <IconButton
        size="small"
        color="primary"
        aria-label="like"
        onClick={toggleLike}
      >
        {isLiked ? <ThumbUpAltRoundedIcon /> : <ThumbUpAltOutlinedIcon />}
      </IconButton>
      <IconButton
        size="small"
        color="primary"
        aria-label="dislike"
        onClick={toggleDisLike}
      >
        {isDisLiked ? (
          <ThumbDownAltRoundedIcon />
        ) : (
          <ThumbDownOffAltOutlinedIcon />
        )}
      </IconButton>
    </Box>
  )
}

export default memo(CharacterControls)
