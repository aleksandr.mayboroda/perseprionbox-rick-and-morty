import {memo} from 'react'
import { Box, IconButton, Tooltip, Badge } from '@mui/material'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { characterSelectors } from '../../../store/character'

import LocalActivityRoundedIcon from '@mui/icons-material/LocalActivityRounded'

const Activity = () => {
  const counter = useSelector(characterSelectors.activityCounter())

  return (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'end',
      }}
    >
      <Tooltip title={`You have ${counter} activities`} color="primary">
        <Badge color="primary" badgeContent={counter} showZero>
          <IconButton color="secondary" component={Link} to="/activity/like">
            <LocalActivityRoundedIcon />
          </IconButton>
        </Badge>
      </Tooltip>
    </Box>
  )
}

export default memo(Activity)
