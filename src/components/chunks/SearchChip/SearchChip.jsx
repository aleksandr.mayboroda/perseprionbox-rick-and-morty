import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  characterSelectors,
  characterOperations,
} from '../../../store/character'

import { Chip } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'

import StyledBox from './StyledBox'

const SearchChip = () => {
  const dispatch = useDispatch()
  const search = useSelector(characterSelectors.getSearchLine())

  const onDelete = useCallback(() => {
    dispatch(characterOperations.clearSearch())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return !search ? null : (
    <StyledBox direction="row" spacing={1}>
      {search && (
        <Chip
          label={search}
          onDelete={onDelete}
          deleteIcon={<DeleteIcon />}
          variant="outlined"
          sx={{ textTransform: 'capitalize', padding: '15px 20px' }}
        />
      )}
    </StyledBox>
  )
}

export default SearchChip
