import { memo } from 'react'
import { Outlet } from 'react-router-dom'
import { Box } from '@mui/material'

const ActivityTabsContent = () => {
  return (
    <Box sx={{ py: 10 }}>
      <Outlet />
    </Box>
  )
}

export default memo(ActivityTabsContent)
