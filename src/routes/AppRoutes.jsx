import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'

import { regularRoutes } from './routes'

import Page404 from '../pages/Page404'

const makeRoute = (route) => {
  if (!route.children) {
    return makeTemplate(route)
  } else {
    return makeTemplateWithChildren(route)
  }
}

const makeTemplateWithChildren = ({ path, Component, redirect, children }) => (
  <Route
    key={path}
    path={path}
    element={redirect ? <Navigate to={redirect} /> : <Component />}
  >
    {children.map((child) => makeRoute(child))}
  </Route>
)
const makeTemplate = ({ path, Component, redirect }) => (
  <Route
    key={path}
    path={path}
    element={redirect ? <Navigate to={redirect} /> : <Component />}
  />
)

const AppRoutes = () => {
  const routesList =
    !regularRoutes.length > 0 ? null : regularRoutes.map(makeRoute)

  return (
    <Routes>
      {routesList}
      <Route path="*" element={<Page404 />} />
    </Routes>
  )
}

export default AppRoutes
