import { HOME, CHARACTERS, ACTIVITY, LIKE, DISLIKE } from './consts'

import CharactersPage from '../pages/CharactersPage'
import OneCharacterPage from '../pages/OneCharacterPage'
import ActivityPage from '../pages/ActivityPage'

import Likes from '../components/containers/Likes'
import Dislikes from '../components/containers/Dislikes'

export const regularRoutes = [
  {
    path: HOME,
    Component: '',
    redirect: CHARACTERS,
  },
  {
    path: CHARACTERS,
    Component: CharactersPage,
    redirect: null,
  },
  {
    path: CHARACTERS + '/:id',
    Component: OneCharacterPage,
    redirect: null,
  },
  {
    path: ACTIVITY,
    Component: ActivityPage,
    redirect: null,
    children: [
      {
        path: LIKE,
        Component: Likes,
        redirect: null,
      },
      {
        path: DISLIKE,
        Component: Dislikes,
        redirect: null,
      },
    ],
  },

]
