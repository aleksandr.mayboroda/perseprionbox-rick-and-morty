import { useCallback } from 'react'
import { Chip } from '@mui/material'

import Check from '@mui/icons-material/Check'
import CloseIcon from '@mui/icons-material/Close'
import QuestionMarkIcon from '@mui/icons-material/QuestionMark'

const useStatusChip = () => {
  const returnChipByStatus = useCallback((status) => {
    status = status.toLowerCase()

    let result = (
      <Chip
        color="info"
        label={
          <span>
            <b>Status:</b> {status}
          </span>
        }
        icon={<QuestionMarkIcon fontSize="small" />}
      />
    )

    if (status === 'dead') {
      result = (
        <Chip
          color="error"
          label={
            <span>
              <b>Status:</b> {status}
            </span>
          }
          icon={<CloseIcon fontSize="small" />}
        />
      )
    } else if (status === 'alive') {
      result = (
        <Chip
          color="success"
          label={
            <span>
              <b>Status:</b> {status}
            </span>
          }
          icon={<Check fontSize="small" />}
        />
      )
    }
    return result
  }, [])
  
  return { returnChipByStatus }
}

export default useStatusChip
